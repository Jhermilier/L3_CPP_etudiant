#include "figuregeometrique.h"

FigureGeometrique::FigureGeometrique (Couleur const & couleur) : _couleur(couleur){}

Couleur const & FigureGeometrique::getCouleur() const{
    return _couleur;
}

FigureGeometrique::~FigureGeometrique(){}

void FigureGeometrique::afficher()const{}

