#include "polygoneregulier.h"
#include "couleur.h"
#include "point.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Grouppoly) { };

TEST(Grouppoly, poly_test1)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly(c, p,50,5);
    CHECK_EQUAL(poly.getCouleur()._b, c._b);
    CHECK_EQUAL(poly.getCouleur()._g, c._g);
    CHECK_EQUAL(poly.getCouleur()._r, c._r);
}

TEST(Grouppoly, poly_test2)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly (c,p,50,5);
    CHECK_EQUAL(poly.getNbPoints(), 5);
}

TEST(Grouppoly, poly_test3)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly (c,p,50,5);
    Point p0{150,200};
    CHECK_EQUAL(poly.getPoint(0)._x, p0._x);
    CHECK_EQUAL(poly.getPoint(0)._y, p0._y);
}

TEST(Grouppoly, poly_test4)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly (c,p,50,5);
    Point p1{115,247};
    CHECK_EQUAL(poly.getPoint(1)._x, p1._x);
    CHECK_EQUAL(poly.getPoint(1)._y, p1._y);
}

TEST(Grouppoly, poly_test5)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly(c,p,50,5);
    Point p2{59,229};
    CHECK_EQUAL(poly.getPoint(2)._x, p2._x);
    CHECK_EQUAL(poly.getPoint(2)._y, p2._y);
}

TEST(Grouppoly, poly_test6)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly (c,p,50,5);
    Point p3{59,170};
    CHECK_EQUAL(poly.getPoint(3)._x, p3._x);
    CHECK_EQUAL(poly.getPoint(3)._y, p3._y);
}

TEST(Grouppoly, poly_test7)  {
    Couleur c{1.0,0.0,0.0};
    Point p {100,200};
    PolygoneRegulier poly (c,p,50,5);
    Point p4{115,152};
    CHECK_EQUAL(poly.getPoint(4)._x, p4._x);
    CHECK_EQUAL(poly.getPoint(4)._x, p4._y);
}

