#include <iostream>
#include "point.h"
#include "couleur.h"
#include "ligne.h"
#include "polygoneregulier.h"
#include <gtkmm.h>
#include "viewerfigures.h"

int main (int argc, char ** argv){
    ViewerFigures v (argc, argv);
    v.run();
    return 0;
}
