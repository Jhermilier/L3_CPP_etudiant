#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H
#include <gtkmm.h>
#include <iostream>
#include <vector>
#include "figuregeometrique.h"

class ZoneDessin : public Gtk::DrawingArea{
private:
    std::vector<FigureGeometrique*> _figures;
public:
    ZoneDessin();
    ~ZoneDessin();
protected:
    bool on_draw( GdkEventExpose * event);
    bool gererClic(GdkEventButton * event);
};

#endif // ZONEDESSIN_H
