#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H

#include "couleur.h"

class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique( const Couleur & couleur);
    Couleur const & getCouleur() const;
    void virtual afficher()const;
    ~FigureGeometrique();
};

#endif // FIGUREGEOMETRIQUE_H
