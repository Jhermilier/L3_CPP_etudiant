#include "viewerfigures.h"

ViewerFigures::ViewerFigures(int argc, char** argv){
    _window.set_title("titre");
    _window.set_default_size(640,480);
    _window.add(_dessin);
    _window.show_all();
}

void ViewerFigures::run(){
    _kit.run(_window);
}
