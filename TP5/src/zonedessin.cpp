#include "zonedessin.h"
#include "ligne.h"
#include "couleur.h"
#include "point.h"

ZoneDessin::ZoneDessin(){
    Point p0 {0,0}, p1{100,100};
    Couleur c {1.0,0.0,0.0};
    Ligne * l= new Ligne(c,p0,p1);
    Ligne * l1= new Ligne (c,p1,p0);
    _figures.push_back(l);
    _figures.push_back(l1);
}

ZoneDessin::~ZoneDessin(){
    for(int i=0; i<_figures.size();i++){
        delete _figures.at(i);
    }
}

bool ZoneDessin::on_draw( GdkEventExpose * event){
    for(int i=0;i<_figures.size();i++){
        _figures.at(i)->afficher();
    }
    return true;
}
bool ZoneDessin::gererClic(GdkEventButton * event){return true;}
