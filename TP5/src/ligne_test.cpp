#include "ligne.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Groupligne) { };

TEST(Groupligne, ligne_test1)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getP0()._x,p0._x);
    CHECK_EQUAL(l.getP0()._y, p0._y);
}

TEST(Groupligne, ligne_test2)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getP1()._x,p1._x);
    CHECK_EQUAL(l.getP1()._y, p1._y);
}

TEST(Groupligne, ligne_test3)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getCouleur()._b, c._b);
    CHECK_EQUAL(l.getCouleur()._g, c._g);
    CHECK_EQUAL(l.getCouleur()._r, c._r);
}
