#ifndef IMAGE_H
#define IMAGE_H

#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

class Image{
private:
    int _largeur;
    int _hauteur;
    int * _pixels;
public:
    Image(int largeur, int hauteur);
    Image(Image & img);
    ~Image();

    const Image & operator=(const Image & img );


    int getLargeur()const;
    int getHauteur()const;

    int getPixel(int i, int j)const;
    void setPixel(int i, int j, int couleur);

    const int & pixel(int i, int j)const;
    int & pixel(int i, int j);
};

void ecrirePnm(const Image & i, const std::string & nomFichier);
void remplir (Image & i);
Image bordure (const Image & img, int couleur);

#endif // IMAGE_H
