#include "image.h"


Image::Image(int largeur, int hauteur): _largeur(largeur),_hauteur(hauteur){
    _pixels= new int [_largeur*_hauteur];
    for(int i=0;i<_largeur*_hauteur;i++){
        _pixels[i]=0;
    }
}
Image::Image(Image &img): _largeur(img.getLargeur()), _hauteur(img.getHauteur()) {
    _pixels = new int [_largeur*_hauteur];
    for(int i=0;i<_largeur*_hauteur;i++){
            _pixels[i]=img._pixels[i];
    }
}
Image::~Image(){
    delete [] _pixels;
}

const Image & Image:: operator=(const Image & img ){
   if(&img != this){
        _hauteur=img._hauteur;
        _largeur=img._largeur;
        delete []_pixels;
        _pixels = new int [_largeur*_hauteur];

        for(int i=0;i<_largeur*_hauteur;i++){
                _pixels[i]=img._pixels[i];
        }
    }
    return *this;
}

int Image::getLargeur()const{
    return _largeur;
}
int Image::getHauteur()const{
    return _hauteur;
}

int Image::getPixel(int i, int j)const{
    return _pixels[i*_largeur+j];
}
void Image::setPixel(int i, int j, int couleur){
    _pixels[i*_largeur+j]=couleur;
}

int & Image::pixel(int i, int j){
    return _pixels[i*_largeur+j];
}
const int & Image::pixel(int i, int j)const{
    return _pixels[i*_largeur+j];
}

void ecrirePnm(const Image & i, const std::string & nomFichier){
    std::ofstream ofs(nomFichier);
    ofs << "P2" << std::endl;
    ofs << i.getLargeur() << " " << i.getHauteur() << std::endl;
    ofs << "255" << std::endl;

    for(int k=0; k<i.getHauteur();k++){
        for( int j=0; j<i.getLargeur();j++){
            ofs<<i.pixel(k,j) << " ";
        }
        ofs << std::endl;
    }
}
void remplir(Image & img){
    double l = img.getLargeur();
    for(int j=0; j<img.getLargeur();j++){
        double t = 2*M_PI*j/l;
        int c = (cos(t)+1)*127;
        for(int i=0; i<img.getHauteur();i++){
            img.pixel(i,j)=c;
        }
    }
}
Image bordure (const Image & img, int couleur){
    Image img2 = img;

    const int il=img2.getLargeur()-1;
    for(int i=0; i<img2.getHauteur(); i++){
        img2.pixel(i,0)=couleur;
        img2.pixel(i,il)=couleur;
    }
    const int jl=img2.getHauteur()-1;
    for(int j=0; j<img2.getLargeur(); j++){
        img2.pixel(0,j)=couleur;
        img2.pixel(jl,j)=couleur;
    }
    return img2;
}
