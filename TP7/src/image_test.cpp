#include "image.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) {};

TEST(GroupImage, Image1){
    Image i(40,30);
    CHECK_EQUAL(i.getLargeur(),40);
    CHECK_EQUAL(i.getHauteur(),30);
}
TEST(GroupImage, Image2){
    Image i(40,30);
    i.setPixel(20,10,255);
    CHECK_EQUAL(i.getPixel(20,10),255);
}

TEST(GroupImage, Image3){
    Image i(40,30);
    i.pixel(20,10)=255;
    CHECK_EQUAL(i.pixel(20,10),255);
}

