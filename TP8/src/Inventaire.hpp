#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <algorithm>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::vector<Bouteille> _bouteilles;
    void trierNom();
    void trierAnnee();
};
//operateur flux sortie
std::ostream & operator<<(std::ostream & os, const Inventaire & in);
//flux entree
std::istream& operator >>(std::istream& is, Inventaire& in) ;

#endif
