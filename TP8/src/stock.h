#ifndef STOCK_H
#define STOCK_H

#include "Inventaire.hpp"

class Stock{
private:
    std:: map <std::string, float> _produits;
public:
   void recalculerStock(const Inventaire & in );
};

#endif // STOCK_H
