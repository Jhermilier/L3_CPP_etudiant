#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

     chargerInventaire(" ");
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte() const{
    std::ostringstream oss;
    oss<< _inventaire;

    oss<< std::endl;

    Inventaire i2= _inventaire;
    i2.trierNom();
    oss<<i2;

    return oss.str();
}

void Controleur::chargerInventaire(const std::string & nomFichier){
   std::ifstream ifs(nomFichier);
   if(!ifs){
       std::cerr<<"echec a l'ouverture du fichier\n";
   }
   //_inventaire.clear();
   ifs>>_inventaire;
   for (auto & v : _vues)
      v->actualiser();
}


