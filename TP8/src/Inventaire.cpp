#include "Inventaire.hpp"

void Inventaire::trierNom(){
    std::sort(_bouteilles.begin(),_bouteilles.end(),cmpNom);
}
void Inventaire::trierAnnee(){
    auto cmpAnnee = [](const Bouteille & b1, const Bouteille & b2) -> bool {
        return b1._date<b2._date;
    };
     std::sort(_bouteilles.begin(),_bouteilles.end(),cmpAnnee);
}


std::ostream & operator<<(std::ostream & os, const Inventaire & in){

    /*for(const Bouteille & b:in._bouteilles){
        os<<b
    for(auto it=in._bouteilles.begin(); it!=in._bouteilles.end();++it){
    os<<*it;
    }*/
    auto f = [&os](const Bouteille & b){
        os<<b;
    };
    std::for_each(in._bouteilles.begin(),in._bouteilles.end(),f);
    return os;
}


std::istream& operator >>(std::istream& is, Inventaire& in) {
    Bouteille b;
    while (is>>b){
        in._bouteilles.push_back(b);
    }
    return is;
}


