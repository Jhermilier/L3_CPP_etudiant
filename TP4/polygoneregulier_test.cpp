#include "polygoneregulier.h""

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Grouppoly) { };

TEST(Grouppoly, poly_test1)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    CHECK_EQUAL(p.getCouleur(), c);
}

TEST(Grouppoly, poly_test2)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    CHECK_EQUAL(p.getNbPoints(), 5);
}

TEST(Grouppoly, poly_test3)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    Point p0(150,200);
    CHECK_EQUAL(p.getPoint(0), p0);
}

TEST(Grouppoly, poly_test4)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    Point p1(115,247);
    CHECK_EQUAL(p.getPoint(1), p1);
}

TEST(Grouppoly, poly_test5)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    Point p2(59,229);
    CHECK_EQUAL(p.getPoint(2), p2);
}

TEST(Grouppoly, poly_test6)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    Point p3(59,170);
    CHECK_EQUAL(p.getPoint(3), p3);
}

TEST(Grouppoly, poly_test7)  {
    Couleur c(1.0,0.0,0.0);
    Point p (100,200);
    PolygoneRegulier p (c,p,50,5);
    Point p4(115,152);
    CHECK_EQUAL(p.getPoint(4), p4);
}

