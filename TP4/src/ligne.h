#ifndef LIGNE_H
#define LIGNE_H
#include "point.h"
#include "couleur.h"
#include "figuregeometrique.h"

class Ligne : public FigureGeometrique
{
private:
    Point _p0,_p1;
public:
    Ligne(const Couleur & couleur, const Point & p0, const Point & p1);
    void afficher()const override;
    Point const & getP0() const;
    Point const & getP1() const;
    ~Ligne();
};

#endif // LIGNE_H
