#include "figuregeometrique.h""

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Groupfigure) { };

TEST(Groupfigure, figure_test1)  {
    Couleur c{1.0,0.0,0.0};
    FigureGeometrique f(c);
    CHECK_EQUAL(f.getCouleur(), c);
}

