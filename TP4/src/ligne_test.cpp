#include "ligne.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(Groupligne) { };

TEST(Groupligne, ligne_test1)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getP0(), p0);
}

TEST(Groupligne, ligne_test2)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getP1(), p1);
}

TEST(Groupligne, ligne_test3)  {
    Couleur c{1.0,0.0,0.0};
    Point p0{0,0};
    Point p1{100,100};
    Ligne l(c,p0,p1);
    CHECK_EQUAL(l.getCouleur(), c);
}
