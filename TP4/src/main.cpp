#include <iostream>
#include "point.h"
#include "couleur.h"
#include "ligne.h"
#include "polygoneregulier.h"
#include <vector>

int main (){
    Couleur r {1.0,0.0,0.0};
    Point p1{ 0,0};
    Point p2{100,200};
    Ligne l(r,p1,p2);
    l.afficher();

    PolygoneRegulier pr (r,p2,50,3);
    pr.afficher();

    std::cout<<"test polymorphisme"<<std::endl;
    /*std::vector <FigureGeometrique> v;
    v.push_back(l);
    v.push_back(pr);
    for(FigureGeometrique p :v){
           p.afficher();
    }*/

        return 0;
}
