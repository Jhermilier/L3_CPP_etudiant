#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H
#include "point.h"
#include "couleur.h"
#include "figuregeometrique.h"


class PolygoneRegulier : public FigureGeometrique
{
private:
    int _nbpoints;
    Point * _points;
public:
    PolygoneRegulier(const Couleur & couleur, const Point & centre,int rayon,int nbCotes);
    void afficher()const override;
    int getNbPoints()const;
    const Point getPoint(int indice)const;
    ~PolygoneRegulier();
};

#endif // POLYGONEREGULIER_H
