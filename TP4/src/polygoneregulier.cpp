#include "polygoneregulier.h"
#include <cmath>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre,int rayon,int nbCotes):
    FigureGeometrique(couleur), _nbpoints(nbCotes){
    _points = new   Point[_nbpoints];
    for(int i=0; i<_nbpoints;i++){
        double angle= i*M_PI*2/double(_nbpoints);
        int x = rayon* cos(angle)+centre._x;
        int y = rayon *sin(angle)+centre._y;
        _points[i]= {x,y};
    }
}

void PolygoneRegulier::afficher()const {
    std::cout<<"polygoneRegulier "<<_couleur._r<<"_"<<_couleur._g<<"_"<<_couleur._b<<" ";
    for(int i=0;i<getNbPoints();i++){
       std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
    }
    std::cout<<std::endl;
}
int PolygoneRegulier::getNbPoints()const{
    return _nbpoints;
}
const Point PolygoneRegulier::getPoint(int indice)const{
    return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
        delete [] _points;
}
