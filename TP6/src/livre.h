#ifndef LIVRE_H
#define LIVRE_H
#include <iostream>

class Livre
{
private:
    std::string _titre;
    std::string _auteur;
    int _annee;
public:
    Livre();
    Livre(const std::string & titre,  const std::string & auteur, int annee);
    const std::string & getTitre() const;
    const std::string & getAuteur() const;
    int getAnnee()const;
    bool operator < (const Livre & l);
    bool operator == (const Livre & l);
    friend std::ostream & operator << (std::ostream & os, const Livre & l);
    friend std::istream & operator >> (std::istream & is,  Livre & l);
};

#endif // LIVRE_H
