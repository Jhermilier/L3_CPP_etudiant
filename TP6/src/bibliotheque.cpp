#include "bibliotheque.h"

void Bibliotheque::afficher()const{
    for(const Livre & l: *this){
        std::cout<<l<<std::endl;
    }
}
void Bibliotheque::trierParAuteurEtTitre(){
    std::sort (begin(),end());
}
void Bibliotheque::trierParAnnee(){
    auto tri= [] (Livre l1, Livre l2){
       return l1.getAnnee()<l2.getAnnee();
    };
    std::sort(begin(),end(), tri);
}
void Bibliotheque::lireFichier(const std::string & nomFichier){
    std::ifstream is (nomFichier);
        Livre l;
        while(is>>l){
            push_back(l);
        }
}
void Bibliotheque::ecrireFichier(const std::string & nomFichier)const {
    std::ofstream os(nomFichier);
        for(const  Livre & l : *this){
            os<<l<<std::endl;
}}
