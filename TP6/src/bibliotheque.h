#ifndef BIBLIOTHEQUE_H
#define BIBLIOTHEQUE_H
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include "livre.h"

class Bibliotheque : public std::vector<Livre>{
public:
    void afficher()const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(const std::string & nomFichier);
    void ecrireFichier(const std::string & nomFichier)const ;
};

#endif // BIBLIOTHEQUE_H
