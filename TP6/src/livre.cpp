#include "livre.h"

Livre::Livre (){}
Livre::Livre(const std::string & titre,  const std::string & auteur, int annee) : _titre(titre),_auteur(auteur) , _annee(annee){

    if (_auteur.find(";")!=std::string::npos) throw std::string("erreur : auteur non valide (';' non autorisé)");
    if (_auteur.find("\n")!=std::string::npos) throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    if (_titre.find(";")!=std::string::npos ) throw std::string("erreur : titre non valide (';' non autorisé)");
    if (_titre.find(";")!=std::string::npos) throw std::string("erreur : titre non valide ('\n' non autorisé)");
}
const std::string & Livre::getTitre() const{
    return _titre;
}
const std::string & Livre::getAuteur()const{
    return _auteur;
}
int Livre::getAnnee()const{
    return _annee;
}
bool Livre::operator < (const Livre & l){
    if( _auteur<l.getAuteur()) return true;
    else if (_auteur == l.getAuteur() && _titre <l.getTitre()) return true;
    return false;
}
bool Livre::operator == (const Livre & l){
    return (_auteur== l.getAuteur()&& _titre==l.getTitre() && _annee==l.getAnnee());
}
std::ostream & operator << (std::ostream& os, const Livre & l){
    os<<l.getTitre()<<";"<<l.getAuteur()<<";"<<l.getAnnee();
    return os;
}
std::istream & operator >> (std::istream & is, Livre & l){
    std::string annee;
    std::getline(is,l._titre,';');
    std::getline(is,l._auteur,';');
    std::getline(is,annee,';');
    l._annee= std::stoi(annee);
    return is;
}
