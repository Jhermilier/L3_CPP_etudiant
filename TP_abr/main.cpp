#include "vector.hpp"

#include <chrono>
#include <iostream>
#include <random>

using timepoint = std::chrono::time_point<std::chrono::system_clock>;

double duration(timepoint t0, timepoint t1) {
    std::chrono::duration<double> nbSeconds(t1 - t0);
    return nbSeconds.count();
}

int main() {

    const int NB_SEARCH = 100000;
    const std::vector<int> SIZES { 100, 1000, 10000, 100000 };

    std::mt19937 engine(std::random_device{}());

    for (int size : SIZES) {
        std::uniform_int_distribution<int> dist(1, size);

        // generate random vector/tree
        std::vector<int> v(size);
        // TODO tree
        for (int s=0; s<size; s++) {
            const int x = dist(engine);
            v[s] = x;
        }

        // search in vector
        std::mt19937 engine_vector(engine);
        auto t0_vector = std::chrono::system_clock::now();
        for (int k=0; k<NB_SEARCH; k++) {
            const int x = dist(engine_vector);
            search(v, x);
        }
        auto t1_vector = std::chrono::system_clock::now();

        // search in tree
        // TODO

        // test vector/tree
        for (int k=0; k<NB_SEARCH; k++) {
            const int x = dist(engine);
            bool found_vector = search(v, x);
            // TODO
            /*
            bool found_tree = t.search(x);
            if (found_vector != found_tree) {
                std::cout << found_vector << " != " << found_tree << std::endl;
                std::cout << x << std::endl;
                // print(v);
                // print(t);
                exit(-1);
            }
            */
        }

        // output results
        // TODO
        std::cout << size << " "
                << duration(t0_vector, t1_vector) << std::endl;
    }

    return 0;
}

